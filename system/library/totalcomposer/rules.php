<?php

	namespace TotalComposer;

	class Rules {
		private $registry;

		public function __construct($registry)
		{
			$this->registry = $registry;

			$this->db = $registry->get('db');
		}

		public function get($id = null) {
			$sql = "SELECT * FROM " . DB_PREFIX . "total_composer";

			if($id !== null) {
				$sql .= " WHERE id = '" . (int)$id . "'";
			}

			$query = $this->db->query($sql);

			return $query->rows;
		}

		public function add() {

		}

		public function update($id) {

		}

		public function delete($id) {

		}
	}