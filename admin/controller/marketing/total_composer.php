<?php

	use TotalComposer\Rules;

	class ControllerMarketingTotalComposer extends Controller
	{
		private $error = array();

		public function index()
		{
			$this->load->language('marketing/total_composer');

			$this->document->setTitle($this->language->get('heading_title'));

			$this->loadView('list');
		}

		public function form()
		{
			$this->load->language('marketing/total_composer');

			$this->document->setTitle($this->language->get('heading_title'));

			$this->loadView('form');
		}

		public function rules()
		{
			$this->load->model('marketing/total_composer');
			$this->load->language('marketing/total_composer');

			$method = $this->request->server['REQUEST_METHOD'];

			$rulesClass = new Rules($this->registry);
			$data = [];

			switch ($method) {
				case "GET":
					$data['rules'] = $rulesClass->get();

					$limit = $this->config->get('config_limit_admin');

					if (isset($this->request->get['page'])) {
						$page = $this->request->get['page'];
					} else {
						$page = 1;
					}

					$filter_data = array(
						'start' => ($page - 1) * $limit,
						'limit' => $limit
					);

					$items_total = $this->model_marketing_total_composer->getTotalRules();

					$data['pagination'] = array(
						'length' => ceil($items_total / $limit),
						'limit' => $limit,
						'page' => $page,
						'result_text' => sprintf($this->language->get('text_pagination'), ($items_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($items_total - $limit)) ? $items_total : ((($page - 1) * $limit) + $limit), $items_total, ceil($items_total / $limit)),
						'total' => $items_total
					);

					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($data));
					break;
				case "POST":
					$this->addRules();
					break;
				case "PUT":
					$this->editRules();
					break;
				case "DELETE":
					$this->deleteRules();
					break;
				default:
					return null;
			}
		}

		protected function loadView($page)
		{
			$this->document->addScript('view/javascript/codemaniac/dist/chunk-vendors.js');
			$this->document->addScript('view/javascript/codemaniac/dist/total-composer.bundle.js');

			$data['language'] = json_encode($this->load->language('marketing/total_composer'));

			$data['session'] = json_encode(array(
				'userToken' => $this->session->data['user_token'],
				'currency' => $this->config->get('config_currency')
			));

			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');

			$this->response->setOutput($this->load->view('marketing/total_composer_' . $page, $data));
		}

		public function getRules()
		{
			$data = array();

			if (isset($this->request->get['id'])) {
				$id = $this->request->get['id'];
				$rule = $this->model_marketing_total_composer->getRule($id);

				$data['rule'] = array(
					'id' => $rule['id'],
					'name' => $rule['name'],
					'sort_order' => $rule['sort_order'],
					'priority' => $rule['priority'],
					'status' => $rule['status'],
					'settings' => json_decode(htmlspecialchars_decode($rule['settings'])),
					'date_modified' => date($this->language->get('date_format_short'), strtotime($rule['date_modified'])),
					'date_added' => date($this->language->get('date_format_short'), strtotime($rule['date_added'])),
				);
			} else {
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}

				$limit = $this->config->get('config_limit_admin');

				$filter_data = array(
					'start' => ($page - 1) * $limit,
					'limit' => $limit
				);

				$items_total = $this->model_marketing_total_composer->getTotalRules();
				$rules = $this->model_marketing_total_composer->getRules($filter_data);

				foreach ($rules as $rule) {
					$data['rules'][] = array(
						'id' => $rule['id'],
						'name' => $rule['name'],
						'sort_order' => $rule['sort_order'],
						'priority' => $rule['priority'],
						'status' => $rule['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
						'date_modified' => date($this->language->get('date_format_short'), strtotime($rule['date_modified'])),
						'date_added' => date($this->language->get('date_format_short'), strtotime($rule['date_added'])),
					);
				}

				$data['pagination'] = array(
					'length' => ceil($items_total / $limit),
					'limit' => $limit,
					'page' => $page,
					'result_text' => sprintf($this->language->get('text_pagination'), ($items_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($items_total - $limit)) ? $items_total : ((($page - 1) * $limit) + $limit), $items_total, ceil($items_total / $limit)),
					'total' => $items_total
				);
			}


			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
		}

		protected function addRules()
		{
			$data = json_decode(file_get_contents("php://input"), true);

			$this->model_marketing_total_composer->addRule($data);

			$response = [
				'status' => 'success',
				'message' => 'test'
			];

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($response));
		}

		public function editRules()
		{
			$this->load->language('marketing/total_composer');

			$this->document->setTitle($this->language->get('heading_title'));

			$this->load->model('marketing/total_composer');

			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

				$this->model_marketing_total_composer->editRule($this->request->get['id'], $this->request->post);

				$this->session->data['success'] = $this->language->get('text_success');

				$url = '';

				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}

				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}

				$this->response->redirect($this->url->link('marketing/total_composer', 'user_token=' . $this->session->data['user_token'] . $url, true));
			}

			$this->getForm();
		}

		protected function deleteRules()
		{
			$response = array();

			$data = json_decode(file_get_contents("php://input"), true);
			$ids = $data['ids'] ?? null;


			if (!$ids || !is_array($ids)) {
				$response['error'] = $this->language->get('error_bad_params');
			} else if (!$this->validateDelete()) {
				$response['error'] = $this->language->get('error_permission');
			} else {
				foreach ($ids as $id) {
					if (!$this->model_marketing_total_composer->deleteRule($id)) {
						$response['error'] = $this->language->get('error_not_completed');
					}
				}

				if(!isset($response['error'])) {
					$response['success'] = $this->language->get('text_success');
				}
			}

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($response));
		}



		public function presets()
		{
			$method = $this->request->server['REQUEST_METHOD'];
			$this->load->model('marketing/total_composer');

			switch ($method) {
				case "GET":
					$this->getPresets();
					break;
				case "POST":
					$this->addPreset($this->request->post);
					break;
				case "PUT":
					$this->editRules();
					break;
				case "DELETE":
					$this->deleteRules();
					break;
				default:
					return null;
			}
		}

		protected function getPresets()
		{
			$result = [];

			$presets = $this->model_marketing_total_composer->getPresets();

			foreach ($presets as $preset) {
				$result[] = array(
					'id' => $preset['id'],
					'name' => $preset['name'],
					'setting' => json_decode(htmlspecialchars_decode($preset['setting'])),
					'date_modified' => date($this->language->get('date_format_short'), strtotime($preset['date_modified'])),
					'date_added' => date($this->language->get('date_format_short'), strtotime($preset['date_added'])),
				);
			}

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($result));
		}

		protected function addPreset($preset)
		{
			$this->model_marketing_total_composer->addPreset($preset);
		}

		public function getEssences()
		{
			if (isset($this->request->get['type'])) {
				switch ($this->request->get['type']) {
					case "products":
						$this->load->model('catalog/product');
						$this->load->model('marketing/total_composer');
						$this->getEssence("product");
						break;
					case "categories":
						$this->load->model('marketing/total_composer');
						$this->getEssence("category");
						break;
					case "options":
						$this->load->model('marketing/total_composer');
						$this->getEssence("option");
						break;
					case "manufacturers":
						$this->load->model('marketing/total_composer');
						$this->getEssence("manufacturer");
						break;
					default:
						$this->error['warning'] = $this->language->get('error_permission');
						return !$this->error;
						break;
				}
			} else {
				// Вставить верную ошибку
				$this->error['warning'] = $this->language->get('error_permission');
				return !$this->error;
			}
		}

		protected function getEssence($table_name)
		{
			if (isset($this->request->get['query'])) {
				$query = $this->request->get['query'];
			} else {
				$query = '';
			}

			if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
			} else {
				$page = 1;
			}

			$limit = 10;

			$data['items'] = array();

			$filter_data = array(
				'search_query' => $query,
				'start' => ($page - 1) * $limit,
				'limit' => $limit
			);

			$items_total = $this->model_marketing_total_composer->getTotalEssences($table_name, $filter_data);

			$results = $this->model_marketing_total_composer->getEssences($table_name, $filter_data);

			if ($table_name === "product") {
				foreach ($results as $result) {
					$data['items'][] = array(
						'id' => $result['product_id'],
						'name' => $result['name'],
						'model' => $result['model'],
						'price' => $this->currency->format($result['price'], $this->config->get('config_currency')),
						'quantity' => $result['quantity'],
						'status' => $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
					);
				}
			} elseif ($table_name === "category") {
				foreach ($results as $result) {
					$data['items'][] = array(
						'id' => $result['category_id'],
						'name' => $result['name'],
						'status' => $result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
					);
				}
			} elseif ($table_name === "manufacturer") {
				foreach ($results as $result) {
					$data['items'][] = array(
						'id' => $result['manufacturer_id'],
						'name' => $result['name'],
					);
				}
			}

			$data['pagination'] = array(
				'total' => $items_total
			);

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));
		}

		public function getShippingMethods()
		{
			$this->load->model('setting/extension');

			$extensions = $this->model_setting_extension->getInstalled('shipping');

			foreach ($extensions as $key => $value) {
				if (!is_file(DIR_APPLICATION . 'controller/extension/shipping/' . $value . '.php') && !is_file(DIR_APPLICATION . 'controller/shipping/' . $value . '.php')) {
					$this->model_setting_extension->uninstall('shipping', $value);

					unset($extensions[$key]);
				}
			}

			$data['items'] = array();

			// Compatibility code for old extension folders
			$files = glob(DIR_APPLICATION . 'controller/extension/shipping/*.php');

			if ($files) {
				foreach ($files as $file) {
					$extension = basename($file, '.php');

					$this->load->language('extension/shipping/' . $extension, 'extension');

					if (in_array($extension, $extensions)) {
						$data['items'][] = array(
							'text' => $this->language->get('extension')->get('heading_title'),
							'value' => $extension
						);
					}
				}
			}
			echo json_encode($data);
		}

		public function getPaymentMethods()
		{
			$this->load->model('setting/extension');

			$extensions = $this->model_setting_extension->getInstalled('payment');

			foreach ($extensions as $key => $value) {
				if (!is_file(DIR_APPLICATION . 'controller/extension/payment/' . $value . '.php') && !is_file(DIR_APPLICATION . 'controller/payment/' . $value . '.php')) {
					$this->model_setting_extension->uninstall('payment', $value);

					unset($extensions[$key]);
				}
			}

			$data['items'] = array();

			// Compatibility code for old extension folders
			$files = glob(DIR_APPLICATION . 'controller/extension/payment/*.php');

			if ($files) {
				foreach ($files as $file) {
					$extension = basename($file, '.php');

					$this->load->language('extension/payment/' . $extension, 'extension');

					if (in_array($extension, $extensions)) {
						$data['items'][] = array(
							'text' => $this->language->get('extension')->get('heading_title'),
							'value' => $extension
						);
					}
				}
			}

			echo json_encode($data);
		}

		public function getCountries()
		{
			$data['items'] = array();

			$this->load->model('localisation/country');

			$results = $this->model_localisation_country->getCountries();

			foreach ($results as $result) {
				$data['items'][] = array(
					'id' => $result['country_id'],
					'name' => $result['name'] . (($result['country_id'] == $this->config->get('config_country_id')) ? $this->language->get('text_default') : null),
					'iso_code_2' => $result['iso_code_2'],
					'iso_code_3' => $result['iso_code_3'],
				);
			}

			echo json_encode($data);
		}

		public function getZones()
		{
			$data['items'] = array();

			$this->load->model('localisation/zone');

			$results = $this->model_localisation_zone->getZones();

			foreach ($results as $result) {
				$data['items'][] = array(
					'id' => $result['zone_id'],
					'country' => $result['country'],
					'name' => $result['name'] . (($result['zone_id'] == $this->config->get('config_zone_id')) ? $this->language->get('text_default') : null),
					'code' => $result['code'],
				);
			}

			echo json_encode($data);
		}

		public function getGeoZones()
		{
			$data['items'] = array();

			$this->load->model('localisation/geo_zone');

			$results = $this->model_localisation_geo_zone->getGeoZones();

			foreach ($results as $result) {
				$data['items'][] = array(
					'id' => $result['geo_zone_id'],
					'name' => $result['name'],
				);
			}

			echo json_encode($data);
		}

		protected function validateForm()
		{
			if (!$this->user->hasPermission('modify', 'marketing/total_composer')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}

			if (utf8_strlen($this->request->post['name']) < 3) {
				$this->error['name'] = $this->language->get('error_code');
			}

//        $rule_info = $this->model_marketing_total_composer->getRule($this->request->post['id']);
//
//        if ($rule_info) {
//            if (!isset($this->request->get['id'])) {
//                $this->error['warning'] = $this->language->get('error_exists');
//            } elseif ($rule_info['id'] != $this->request->get['id'])  {
//                $this->error['warning'] = $this->language->get('error_exists');
//            }
//        }

			return !$this->error;
		}

		protected function validateDelete()
		{
			if (!$this->user->hasPermission('modify', 'marketing/total_composer')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}

			return !$this->error;
		}
	}
