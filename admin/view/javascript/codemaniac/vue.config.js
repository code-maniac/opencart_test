module.exports = {
  chainWebpack: config => {
    config.plugins.delete('html-total-composer');
    config.plugins.delete('preload-total-composer');
    config.plugins.delete('prefetch-total-composer');

    config.plugins.delete('html-total-composer-list');
    config.plugins.delete('preload-total-composer-list');
    config.plugins.delete('prefetch-total-composer-list');

    config.plugins.delete('html-total-composer-settings');
    config.plugins.delete('preload-total-composer-settings');
    config.plugins.delete('prefetch-total-composer-settings');
  },

  pages: {
    'total-composer': {
      entry: 'src/Applications/TotalComposer/main.js',
      chunks: ['chunk-vendors', 'chunk-common', '{pagename}'],
      subpage: [
        'src/Applications/TotalComposer/Form/main.js',
        'src/Applications/TotalComposer/List/main.js',
        'src/Applications/TotalComposer/Settings/main.js',
      ],
    },
  },

  configureWebpack: {
    output: {
      filename: '[name].bundle.js',
      chunkFilename: '[name].js',
    },
  },

  filenameHashing: false,
  assetsDir: 'assets',
  publicPath: 'view/javascript/codemaniac/dist/',
  transpileDependencies: ['vuetify'],
  productionSourceMap: false,

  css: {
    loaderOptions: {
      scss: {
        implementation: require('sass'),
        prependData: `
                    @import '@/assets/scss/variables.scss';
                    @import '@/assets/scss/mixins.scss';
                `,
      },
    },
  },
};
