<?php
// HTTP
define('HTTP_SERVER', 'http://total-composer.test/admin/');
define('HTTP_CATALOG', 'http://total-composer.test/');

// HTTPS
define('HTTPS_SERVER', 'https://total-composer.test/admin/');
define('HTTPS_CATALOG', 'https://total-composer.test/');

// DIR
define('DIR_APPLICATION', '/Users/codemaniac/Projects/total-composer/admin/');
define('DIR_SYSTEM', '/Users/codemaniac/Projects/total-composer/system/');
define('DIR_IMAGE', '/Users/codemaniac/Projects/total-composer/image/');
define('DIR_STORAGE', '/Users/codemaniac/Projects/total-composer/storage/');
define('DIR_CATALOG', '/Users/codemaniac/Projects/total-composer/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'opencart');
define('DB_PASSWORD', '4]Ecr^XS');
define('DB_DATABASE', 'opencart');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
