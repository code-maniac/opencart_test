<?php

	class ModelExtensionTotalTotalComposer extends Model
	{

		public function createTable()
		{
			$this->db->query('CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'total_composer` (
                          `rule_id` int(11) NOT NULL,
                          `name` varchar(255) NOT NULL,
                          `priority` int(3) NOT NULL,
                          `setting` text NULL,
                          `sort_order` int(3) NOT NULL,
                          `status` tinyint(1) NOT NULL,
                          `date_added` datetime NOT NULL,
                          `date_modified` datetime NOT NULL,
                          PRIMARY KEY (`rule_id`)
                          ) ENGINE=MyISAM DEFAULT CHARSET=utf8;');
		}
	}