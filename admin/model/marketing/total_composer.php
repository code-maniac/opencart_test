<?php
	class ModelMarketingTotalComposer extends Model {
		public function addRule($data) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "total_composer SET name = '" . $this->db->escape($data['name']) . "', settings = '" . $this->db->escape($data['settings']) . "', sort_order = '" . (int)$data['sort_order'] . "', priority = '" . (int)$data['priority'] . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

			return $this->db->getLastId();
		}

		public function editRule($rule_id, $data) {
			$this->db->query("UPDATE " . DB_PREFIX . "total_composer SET name = '" . $this->db->escape($data['name']) . "', settings = '" . $this->db->escape($data['settings']) . "', sort_order = '" . (int)$data['sort_order'] . "', priority = '" . (int)$data['priority'] . "', status = '" . (int)$data['status'] . "' WHERE id = '" . (int)$rule_id . "'");
		}

		public function deleteRule($rule_id) {
			return $this->db->query("DELETE FROM " . DB_PREFIX . "total_composer WHERE id = '" . (int)$rule_id . "'");
		}

		public function getRule($rule_id) {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "total_composer WHERE id = '" . (int)$rule_id . "'");

			return $query->row;
		}

		public function getRules($data = array()) {
			$sql = "SELECT * FROM " . DB_PREFIX . "total_composer ORDER BY date_added";


			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		}

		public function getTotalRules() {
			$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "total_composer");

			return $query->row['total'];
		}

		public function addPreset($data) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "total_composer_presets SET name = '" . $this->db->escape($data['name']) . "', setting = '" . $this->db->escape($data['setting']) . "', date_added = NOW()");

			return $this->db->getLastId();
		}

		public function getPresets($data = array()) {
			$sql = "SELECT * FROM " . DB_PREFIX . "total_composer_presets ORDER BY date_added";

			$query = $this->db->query($sql);

			return $query->rows;
		}

		public function getEssences($table_name, $data = array()) {

			switch ($table_name){
				case "product":
					$sql = "SELECT * FROM " . DB_PREFIX . $table_name . " e LEFT JOIN " . DB_PREFIX . $table_name . "_description ed ON (e.product_id = ed.product_id) WHERE ed.language_id = '" . (int)$this->config->get('config_language_id') . "'";

					if (!empty($data['search_query'])) {
						$sql .= " AND e.product_id LIKE '" . (int)$data['search_query'] . "'";
						$sql .= " OR ed.name LIKE '" . $this->db->escape($data['search_query']) . "%'";
						$sql .= " OR e.model LIKE '" . $this->db->escape($data['search_query']) . "%'";
						$sql .= " OR e.price LIKE '" . $this->db->escape($data['search_query']) . "%'";
						$sql .= " OR e.quantity = '" . (int)$data['search_query'] . "'";
//						$sql .= " OR e.status = '" . (int)$data['search_query'] . "'";
					}

					$sql .= " GROUP BY e.product_id";

					$sort_data = array(
						'ed.name',
						'e.model',
						'e.price',
						'e.quantity',
						'e.status',
						'e.sort_order'
					);
					break;
				case "category":
					$sql = "SELECT * FROM " . DB_PREFIX . $table_name . " e LEFT JOIN " . DB_PREFIX . $table_name . "_description ed ON (e.category_id = ed.category_id) WHERE ed.language_id = '" . (int)$this->config->get('config_language_id') . "'";

					if (!empty($data['search_query'])) {
						$sql .= " AND ed.name LIKE '" . $this->db->escape($data['search_query']) . "%'";
					}

					$sql .= " GROUP BY e.category_id";

					$sort_data = array(
						'ed.name',
						'e.status',
						'e.sort_order'
					);
					break;
				case "option":
					$sql = "SELECT * FROM " . DB_PREFIX . $table_name . " e LEFT JOIN " . DB_PREFIX . $table_name . "_description ed ON (e.option_id = ed.option_id) WHERE ed.language_id = '" . (int)$this->config->get('config_language_id') . "'";

					if (!empty($data['search_query'])) {
						$sql .= " AND ed.name LIKE '" . $this->db->escape($data['search_query']) . "%'";
					}

					$sql .= " GROUP BY e.option_id";

					$sort_data = array(
						'ed.name',
						'e.status',
						'e.sort_order'
					);
					break;
				case "manufacturer":
					$sql = "SELECT * FROM " . DB_PREFIX . $table_name ;

					if (!empty($data['search_query'])) {
						$sql .= " AND name LIKE '" . $this->db->escape($data['search_query']) . "%'";
					}

					$sql .= " GROUP BY manufacturer_id";

					$sort_data = array(
						'name',
						'sort_order'
					);
					break;
			}
//
//			if (isset($data['sort_order']) && in_array($data['sort_order'], $sort_data)) {
//				$sql .= " ORDER BY " . $data['sort_order'];
//			} else {
//				$sql .= " ORDER BY ed.name";
//			}
//
//			if (isset($data['order']) && ($data['order'] == 'DESC')) {
//				$sql .= " DESC";
//			} else {
//				$sql .= " ASC";
//			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);
			return $query->rows;
		}

		public function getTotalEssences($table_name, $data = array()) {
			switch ($table_name){
				case "product":
					$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . $table_name . " p LEFT JOIN " . DB_PREFIX . $table_name . "_description pd ON (p.product_id = pd.product_id)";
					$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
					break;
				case "category":
					$sql = "SELECT COUNT(DISTINCT p.category_id) AS total FROM " . DB_PREFIX . $table_name . " p LEFT JOIN " . DB_PREFIX . $table_name . "_description pd ON (p.category_id = pd.category_id)";
					$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
					break;
				case "option":
					$sql = "SELECT COUNT(DISTINCT p.option_id) AS total FROM " . DB_PREFIX . $table_name . " p LEFT JOIN " . DB_PREFIX . $table_name . "_description pd ON (p.option_id = pd.option_id)";
					$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
					break;
				case "manufacturer":
					$sql = "SELECT COUNT(DISTINCT manufacturer_id) AS total FROM " . DB_PREFIX . $table_name;
					break;
			}

			if (!empty($data['search_query'])) {
				$sql .= " AND pd.name LIKE '" . $this->db->escape($data['search_query']) . "%'";
//				$sql .= " AND p.model LIKE '" . $this->db->escape($data['filter_model']) . "%'";
//				$sql .= " AND p.price LIKE '" . $this->db->escape($data['filter_price']) . "%'";
//				$sql .= " AND p.quantity = '" . (int)$data['filter_quantity'] . "'";
//				$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
			}

			$query = $this->db->query($sql);

			return $query->row['total'];
		}
	}
