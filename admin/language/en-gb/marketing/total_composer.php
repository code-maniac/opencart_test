<?php
// Heading
	$_['heading_title']             = 'Total Composer';
	$_['form_headline']             = 'Total Composer';
	$_['form_side_headline']        = 'Доступность';
	$_['form_side_presets']         = 'Пресеты';

// Buttons
	$_['button_save']               = 'Сохранить';
	$_['button_cancel']             = 'Отмена';
	$_['button_select']             = 'Выбрать';
	$_['button_back']               = 'Вернуться к списку';
	$_['button_add']               = 'Добавить новое правило';
	$_['button_edit']               = 'Редактировать правило';
	$_['button_delete']               = 'Удалить правило';
	$_['button_delete_selected']               = 'Удалить выбранные правила';
	$_['button_help']               = 'Запросить помощь';
	$_['button_more_modules']       = 'Другие модули автора';
	$_['button_docs']               = 'Документация';
	$_['button_delete_condition']               = 'Удалить условие';
	$_['button_delete_all_condition']               = 'Удалить все условия';
	$_['button_add_condition']               = 'Добавить условие';
	$_['button_save_condition_preset']               = 'Сохранить пресет';
	$_['button_load_condition_preset']               = 'Загрузить пресет';

// Fields
	$_['field_name']                = "Название";
	$_['field_status']              = "Статус";
	$_['field_status_active']       = "Активно";
	$_['field_status_disabled']     = "Отключено";
	$_['field_date_from']           = "Начало активности";
	$_['field_date_to']             = "Окончание активности";
	$_['field_priority']            = "Приоритет";
	$_['field_sort_order']          = "Порядок сортировки";
	$_['field_date_added']          = "Дата создания";
	$_['field_date_modified']       = "Дата изменения";
	$_['field_shipping_method']     = "Метод доставки";
	$_['field_payment_method']      = "Метод оплаты";
	$_['field_country']             = "Страна";
	$_['field_zone']                = "Регион";
	$_['field_geo_zone']            = "Гео зона";
	$_['field_post_code']            = "Почтовый индекс";


//	Tooltips
	$_['tooltip_field_name']        = "Название, которое будет отображено в заказе";
	$_['tooltip_field_date_from']   = "Дата, начиная с которой, правило будет применяться к заказу. По умолчанию – текущая дата.";
	$_['tooltip_field_date_to']     = "Дата, после которой, правило больше не будет применяться к заказу. Если оставить поле пустым, правило будет действовать постоянно.";
	$_['tooltip_field_priority']    = "Приоритет, определяющий, какое правило применится раньше. Чем меньше значение, тем раньше применится правило.";
	$_['tooltip_field_sort_order']  = "Порядок отображения правил в списке. Чем меньше значение, тем выше в списке будет правило.";

// Form Builder
	$_['fb_headline']               = "Выберите действие и настройте одно или несколько условий";


// Text
	$_['text_heading']               = "Выберите действие и настройте для него одно или несколько условий";
	$_['text_in_cart']               = "В корзине";
	$_['text_add']                   = "Добавление правила";
	$_['text_edit']                  = "Изменение правила";
	$_['text_success']               = "Операция выполнена успешно!";
	$_['text_actions']               = "Действия";
	$_['text_general_conditions']    = "Условия";
	$_['text_action_do']             = "Выполнить действие";
	$_['text_yes']                   = "да";
	$_['text_no']                    = "нет";
	$_['text_and']                    = "и";
	$_['text_or']                    = "или";
	$_['text_equal']     = "равно";
	$_['text_not_equal']     = "не равно";
	$_['text_greater']     = "больше";
	$_['text_less']     = "меньше";
	$_['text_greater_than_or_equal']     = "больше либо равно";
	$_['text_less_than_or_equal']     = "меньше либо равно";
	$_['text_pieces']     = "шт.";
	$_['text_contains']     = "содержит";
	$_['text_not_contains']     = "не содержит";
	$_['text_present_any_product']     = "присутствует любой товар из списка";
	$_['text_missing_any_product']     = "отсутствует любой товар из списка";
	$_['text_missing_all_product']     = "присутствуют все товары из списка";
	$_['text_missing_all_product']     = "отсутствуют все товары из списка";
	$_['text_present_any_category']     = "присутствуют товары любой категории из списка";
	$_['text_missing_any_category']     = "отсутствуют товары любой категории из списка";
	$_['text_missing_all_category']     = "присутствуют товары всех категорий из списка";
	$_['text_missing_all_category']     = "отсутствуют товары всех категорий из списка";
	$_['text_present_any_manufacturer']     = "присутствуют товары любго производителя из списка";
	$_['text_missing_any_manufacturer']     = "отсутствуют товары любго производителя из списка";
	$_['text_missing_all_manufacturer']     = "присутствуют товары всех производителей из списка";
	$_['text_missing_all_manufacturer']     = "отсутствуют товары всех производителей из списка";
	$_['text_search_params']     = "Поиск по параметрам";
	$_['text_loading']     = "Подождите, идет загрузка";

// Action
	$_['action_type_default']        = "Выберите действие";
	$_['action_type_price']          = "Изменить стоимость товаров в корзине";
	$_['action_type_shipping']       = "Изменить стоимость доставки";
	$_['action_type_gift']           = "Добавить подарок";
	$_['action_add']                 = "Добавить действие";
	$_['action_delete']              = "Удалить действие";
	$_['action_aggregator_text']              = "к заказам, для которых";
	$_['action_aggregator_all']              = "все условия";
	$_['action_aggregator_any']              = "любое из условий";
	$_['action_aggregator_complete']              = "выполнено(ы)";
	$_['action_aggregator_not_complete']              = "не выполнено(ы)";

	// Cart sum action
	$_['cart_sum_apply_text']        = "применить";
	$_['cart_sum_apply_discount']    = "скидку";
	$_['cart_sum_apply_charge']      = "наценку";
	$_['cart_sum_size']              = "в размере";
	$_['cart_sum_unit_percent']              = "%";
	$_['cart_sum_unit_currency_each']              = "на каждый товар";
	$_['cart_sum_unit_currency_all']              = "на общую сумму товаров";
	$_['cart_sum_max_size']          = "но не более";

// Condition
	$_['condition_type_default']     = "Выберите условие";
	$_['condition_type_order_discount']     = "Были применены скидки";
	$_['condition_type_order_sum']     = "Сумма заказа";
	$_['condition_type_order_products_count']     = "Количество товаров в корзине";
	$_['condition_type_order_shipping_method']     = "Метод доставки";
	$_['condition_type_order_payment_method']     = "Метод оплаты";
	$_['condition_type_order_country']     = "Страна";
	$_['condition_type_order_zone']     = "Регион";
	$_['condition_type_order_geo_zone']     = "Географическая зона";
	$_['condition_type_order_past_code']     = "Почтовый индекс";
	$_['condition_type_product']     = "Товар в корзине";
	$_['condition_type_category']     = "Категория";
	$_['condition_type_price']     = "Цена товара";
	$_['condition_type_manufacturer']     = "Производитель";
	$_['condition_type_name']     = "Название";
	$_['condition_type_model']     = "Модель";
	$_['condition_type_vendorCode']     = "Артикул";
	$_['condition_type_option']     = "Опции";
	$_['condition_type_attribute']     = "Атрибуты";
	$_['condition_type_tag']     = "Теги";
	$_['condition_type_discount']     = "Скидки";
	$_['condition_type_special']     = "Акции";
	$_['condition_type_bonus']     = "Бонусы";
	$_['condition_type_tax']     = "Налог";
	$_['condition_type_quantity']     = "Количество";
	$_['condition_type_need_shipping']     = "Необходима доставка";
	$_['condition_type_sizes']     = "Размеры";
	$_['condition_type_weight']     = "Вес";
	$_['condition_product_popup_title']     = "Выберите товары";
	$_['condition_category_popup_title']     = "Выберите категории";
	$_['condition_manufacturer_popup_title']     = "Выберите производителей";

	//	Condition price
	$_['condition_price_text']     = "Цена любого товара в корзине";
	$_['condition_order_sum_text']     = "Сумма заказа";
	$_['condition_order_products_count_text']     = "Количество товаров в корзине";

	// shipping methods condition
	$_['condition_order_shipping_text']     = "Выбранный метод доставки в заказе";

	// payment methods condition
	$_['condition_order_payment_text']     = "Выбранный метод оплаты в заказе";

	// localisation conditions
	$_['condition_country_text']     = "Страна в заказе";
	$_['condition_zone_text']     = "Регион в заказе";
	$_['condition_geo_zone_text']     = "Географическая зона в заказе";
	$_['condition_post_code_text']     = "Почтовый индекс";


// Column
	$_['column_id']                  = 'id';
	$_['column_name']                = 'Название';
	$_['column_image']               = 'Изображение';
	$_['column_model']               = 'Модель';
	$_['column_price']               = 'Цена';
	$_['column_special']             = 'По скидке';
	$_['column_quantity']            = 'Количество';
	$_['column_status']              = 'Статус';
	$_['column_date_added']          = 'Дата добавления';
	$_['column_date_modified']       = 'Дата изменения';
	$_['column_sort']                = 'Порядок сортировки';
	$_['column_priority']            = 'Приоритет';
	$_['column_action']              = 'Действие';

// Tabs
	$_['tab_setting']                = 'Действия и условия';
	$_['tab_secure']                 = 'Безопасность';

// Entry

	$_['entry_name']                 = 'Название';
	$_['entry_status']               = 'Статус';
	$_['entry_sort']                 = 'Порядок сортировки';
	$_['entry_priority']             = 'Приоритет применимости';
// Help
	$_['help_priority']              = 'чем больше приоритет, тем раньше применится';
	$_['help_sort']                  = 'чем меньше индекс, тем раньше применится';
// Error
	$_['error_warning']              = 'Внимательно проверьте форму на ошибки!!';
	$_['error_permission']           = 'У Вас нет прав для изменения настроек Заказов!';
	$_['error_action']               = 'Не удалось завершить действие!';
	$_['error_bad_params']           = 'Неверные параметры';
	$_['error_not_completed']                  = 'Запрос не может быть выполнен';
