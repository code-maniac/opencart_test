<?php
// Heading
	$_['heading_title']    = 'Total Composer';

// Text
	$_['text_extension']   = 'Extensions';
	$_['text_success']     = 'Success: You have modified cart rules total!';
	$_['text_edit']        = 'Edit Cart Rules Total';

// Entry
	$_['entry_status']     = 'Status';
	$_['entry_sort_order'] = 'Sort Order';

// Error
	$_['error_permission'] = 'Warning: You do not have permission to modify cart rules total!';