<?php
// Heading
	$_['heading_title']              = 'Правила работы с корзиной';
	$_['get_help']                   = 'Запросить помощь';
	$_['documentation']              = 'Документация';

// Text
	$_['text_add']                   = "Добавление правила";
	$_['text_edit']                  = "Изменение правила";
	$_['text_success']               = "Операция выполнена успешно!";
	$_['text_actions']               = "Действия";
	$_['text_general_conditions']    = "Условия";
	$_['text_action_do']             = "Выполнить действие";

// Action
	$_['action_type_default']        = "Выберите действие";
	$_['action_type_price']          = "Изменить стоимость доставки в корзине";
	$_['action_type_shipping']       = "Изменить стоимость доставки";
	$_['action_type_gift']           = "Добавить подарок";

// Condition
	$_['condition_type_default']     = "Выберите условие";

// Column
	$_['column_id']                  = 'id';
	$_['column_name']                = 'Название';
	$_['column_status']              = 'Статус';
	$_['column_date_added']          = 'Дата добавления';
	$_['column_date_modified']       = 'Дата изменения';
	$_['column_sort']                = 'Порядок сортировки';
	$_['column_priority']            = 'Приоритет';
	$_['column_action']              = 'Действие';

// Tabs
	$_['tab_setting']                = 'Действия и условия';
	$_['tab_secure']                 = 'Безопасность';

// Entry

	$_['entry_name']                 = 'Название';
	$_['entry_status']               = 'Статус';
	$_['entry_sort']                 = 'Порядок сортировки';
	$_['entry_priority']             = 'Приоритет применимости';
// Help
	$_['help_priority']              = 'чем больше приоритет, тем раньше применится';
	$_['help_sort']                  = 'чем меньше индекс, тем раньше применится';
// Error
	$_['error_warning']              = 'Внимательно проверьте форму на ошибки!!';
	$_['error_permission']           = 'У Вас нет прав для изменения настроек Заказов!';
	$_['error_action']               = 'Не удалось завершить действие!';
